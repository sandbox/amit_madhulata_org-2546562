<?php
/**
 * @file
 * called functions for the Node Body Ajax module.
 */
function node_body_ajax_admin_settings($form_state) {
  
  $form = array();

  $form['node_body_ajax_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Content types'),
    '#options' => array_map('filter_xss', _node_types_build()->names),
    '#default_value' => variable_get('node_body_ajax_node_types', array()),
    '#description' => t('<b>Select all the content types whose body you would like served via AJAX. This makes it so that users can\'t copy the body or see it via view source.</b>'),
  );
  $form['node_body_ajax_disable_selection'] = array(
    '#type' => 'checkbox',
    '#title' => t('Disable selection'),
    '#default_value' => variable_get('node_body_ajax_disable_selection',FALSE),
    '#description' => t('<b>Attempts to disable selection in JavaScript on a page where a node body is loaded via AJAX.</b>'),
  );
 
  $form['button'] = array(
    '#type' => 'submit',
    '#value' => 'Reset to defaults',
    '#title' => t('Reset Node Body Ajax Setting Form'),
    '#description' => t('Content setting will be reset to default setting.'),
    '#submit' => array('ajax_reset_form'),
    );
  
  return system_settings_form($form);
}

