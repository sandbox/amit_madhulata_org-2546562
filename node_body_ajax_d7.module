<?php
/**
* @file node_body_ajax.module
*
* Core functionality for the Node Body Ajax module.
*/
/**
* Implements hook_help().
*/
function node_body_ajax_d7_help($path, $arg) {
  if ($path == 'admin/help#node_body_ajax_d7') {
    return t('This is a module for Uses Javascript to protect the node body of certain nodes.');
  }
}



/**
 * Implementation of hook_perm().
 */

function node_body_ajax_d7_perm() {
  return array('administer node_body_ajax');
}

/**
 * Implementation of hook_menu().
 */
function node_body_ajax_d7_menu() {
  $items = array();

  $items['admin/appearance/node_body_ajax'] = array(
    'title' => 'Node body ajax settings',
    'description' => 'Settings for protecting the body of certain nodes.',
    'access arguments' => array('administer node_body_ajax'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('node_body_ajax_admin_settings'),
    'file' => 'node_body_ajax_d7.admin.inc',
    'type' => MENU_NORMAL_ITEM,
  );

  $items['node_body_ajax/%node'] = array(
    'page callback' => '_node_body_ajax_callback',
    'page arguments' => array(1),
    'access callback' => 'node_access',
    'access arguments' => array('view', 1),
    'type' => MENU_CALLBACK,
  );

  return $items;
}


function _node_body_ajax_callback($node) {
  return drupal_json_output(array('content' => $node->body['und'][0]['safe_value']));
}

function _node_body_ajax_id() {
  static $counter = 0;
  return 'node-body-ajax' . ($counter++);
}

function _node_body_ajax_enabled($node, $teaser, $page) {
  foreach (module_implements('node_body_ajax') as $module) {
    $func = $module . '_node_body_ajax';
    if ($func($node, $teaser, $page)) {
      return TRUE;
    }
  }
  return FALSE;
}

/**
 * Implementation of hook_preprocess_node().
 */

function node_body_ajax_d7_preprocess_node(&$variables) {
  static $js_added = FALSE;
  $node = $variables['elements']['#node'];
  $teaser = (bool)$variables['teaser'];
  $page = (bool)$variables['page'];
  if (_node_body_ajax_enabled($node, $teaser, $page)) {
    $id = _node_body_ajax_id();
    // replace the content with an empty div
    $variables['content']['body'][0]['#markup'] = '<div class="node-body-ajax" id="' . $id . '"></div>';
    // setup the javascript config
    drupal_add_js(array(
      'node_body_ajax' => array(
        $id => array(
          'nid'    => $node->nid,
          'teaser' => $teaser,
          'page'   => $page,
        )
        )
      ), 'setting');
    // add script to the page
    if (!$js_added) {
      drupal_add_js(drupal_get_path('module', 'node_body_ajax_d7') . '/node_body_ajax_d7.js');
      drupal_add_js(array('node_body_ajax' => array(
        'url' => url('node_body_ajax'),
        'disable_selection' => variable_get('node_body_ajax_disable_selection', FALSE),
      )), 'setting');
      $js_added = TRUE;
    }
  }
}

/**
 * Implementation of hook_node_body_ajax().
 */

function node_body_ajax_d7_node_body_ajax($node) {
  // Use the admin settings
  $node_types = array_filter(variable_get('node_body_ajax_node_types', array()));
  return in_array($node->type, array_values($node_types));
}


function ajax_reset_form($form, &$form_state) {
  $data = '';
  variable_set('node_body_ajax_node_types', NULL);
  variable_set('node_body_ajax_disable_selection', $data);
  drupal_set_message(t('The configuration options have been reset to their default values.'));
}
